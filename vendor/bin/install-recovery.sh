#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:100663296:b6ebf3b5dbe82aa88e0fd250b3ae392cb219f2d4; then
  applypatch  \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:67108864:c29e61618f0472558bf2bae14d8479a4c4c49e35 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:100663296:b6ebf3b5dbe82aa88e0fd250b3ae392cb219f2d4 && \
      log -t recovery "Installing new oppo recovery image: succeeded" && \
      setprop ro.boot.recovery.updated true || \
      log -t recovery "Installing new oppo recovery image: failed" && \
      setprop ro.boot.recovery.updated false
else
  log -t recovery "Recovery image already installed"
  setprop ro.boot.recovery.updated true
fi

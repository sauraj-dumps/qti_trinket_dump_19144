## qssi-user 11 RKQ1.200903.002 1627336434350 release-keys
- Manufacturer: qualcomm
- Platform: trinket
- Codename: trinket
- Brand: qti
- Flavor: qssi-user
- Release Version: 11
- Id: RKQ1.200903.002
- Incremental: 1627336434350
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/trinket/trinket:11/RKQ1.200903.002/1627336434350:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.200903.002-1627336434350-release-keys
- Repo: qti_trinket_dump_19144


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
